{{- if .Values.ingress.enabled -}}
Application should be accessible at

    {{ .Values.service.url }}
{{- else -}}
Application was deployed as internal service 

    {{ .Values.service.name }}

{{- end -}}
